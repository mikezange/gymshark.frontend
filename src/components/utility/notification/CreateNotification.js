import notificationStore from "./NotificationStore";

const createNotification = (type, message) => {
  notificationStore.dispatch(
    "pushNotification",
    {
      type: type,
      message: message
    },
    { root: true }
  );
};

export { createNotification as default };
