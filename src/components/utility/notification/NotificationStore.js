import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const notificationStore = new Vuex.Store({
  state: {
    notifications: []
  },
  mutations: {
    pushNotification: (state, payload) => {
      state.notifications.push(payload);
    },
    removeNotification: (state, notification) => {
      let index = state.notifications.indexOf(notification);
      state.notifications.splice(index, 1);
    }
  },
  actions: {
    pushNotification({ commit }, payload) {
      commit("pushNotification", payload);
    },
    removeNotification({ commit }, notification) {
      commit("removeNotification", notification);
    }
  }
});

export { notificationStore as default };
