module.exports = {
  plugins: {
    "postcss-import": {},
    "postcss-preset-env": {},
    "postcss-nested": {},
    "postcss-for": {},
    tailwindcss: "./tailwind.config.js",
    autoprefixer: {}
  }
};
